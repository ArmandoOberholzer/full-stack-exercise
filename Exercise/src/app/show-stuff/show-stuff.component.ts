import { Component, OnInit } from '@angular/core';
import { FlaskServiceService } from '../flask-service.service';

@Component({
  selector: 'app-show-stuff',
  templateUrl: './show-stuff.component.html',
  styleUrls: ['./show-stuff.component.css']
})
export class ShowStuffComponent implements OnInit {
  thingy: any;
  constructor(private flask: FlaskServiceService) { }

  ngOnInit(): void {
  }

  ShowFlaskExample() {
    this.flask.getExample().toPromise().then(response =>{
      this.thingy = response;
    }).catch(error => console.error(error));
  }
}
